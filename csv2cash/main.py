import piecash
import json
from pathlib import Path
import pandas as pd
from datetime import datetime
from decimal import Decimal
import logging

# TODO
# # Add Log file functionality

logging.basicConfig(
    format=
    '%(asctime)s, %(levelname)-8s [%(filename)s:%(module)s:%(funcName)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.DEBUG)
logger = logging.getLogger(__name__)


def do_csv2cash(path_to_Book, path_to_rawdata, path_to_translationJSON):
    """Performs all csv -> GNUCash operations"""

    logger.info(f'Function start arguments: {locals()}')

    translation = get_translation(path_to_translationJSON)
    rawdata = get_rawdata(path_to_rawdata)
    logger.info(f'Number of rows in rawdata: {len(rawdata.index)}')

    rawdata_prepped = translateandprep_rawdata(translation, rawdata)
    transactions_compiled = compile_transfers(rawdata_prepped, translation['InternalCheckAccounts'])
    import2cash(transactions_compiled, path_to_Book)


def get_compiled_transactions(path_to_rawdata,
                              path_to_translationJSON,
                              returnall=False):
    """Returns DataFrame of compiled transactions"""

    logger.info(f'Function start arguments: {locals()}')

    translation = get_translation(path_to_translationJSON)
    rawdata = get_rawdata(path_to_rawdata)
    rawdata_prepped = translateandprep_rawdata(translation, rawdata)
    transactions_compiled = compile_transfers(rawdata_prepped, translation['InternalCheckAccounts'])

    if not returnall:
        return (transactions_compiled)
    else:
        return (transactions_compiled, translation, rawdata, rawdata_prepped)


def get_uncat_transfers(path_to_rawdata, path_to_translationJSON):
    """Returns DataFrame of uncategorized transfers"""

    logger.info(f'Function start arguments: {locals()}')

    rawdata = get_rawdata(path_to_rawdata)
    translation = get_translation(path_to_translationJSON)

    rawdata = translateandprep_rawdata(translation, rawdata)

    return (rawdata.loc[(rawdata['category_mod'] == 'Uncategorized')])


def write_translation_template(path_to_rawdata,
                               path_to_translationtemplateJSON,
                               path_to_translationJSON=None):
    """Write a template for the tranlsation JSON based on the uncategorized transfers

    Optionally start with an existing translation JSON.
    """

    logger.info(f'Function start arguments: {locals()}')

    rawdata = get_rawdata(path_to_rawdata)

    if path_to_translationJSON:
        translation = get_translation(path_to_translationJSON)
        rawdata = translateandprep_rawdata(translation, rawdata)
        translation_template = translation
        for index, row in rawdata.iterrows():
            if row['category_mod'] == 'Uncategorized':
                translation_template['Categories'][row['Category']] = ''

            if row['account_mod'] == 'Uncategorized':
                translation_template['Accounts'][row['Account Name']] = ''
    else:
        translation_template = {
            'Categories': {
                'Uncategorized': 'Uncategorized'
            },
            'Accounts': {
                'Uncategorized': 'Uncategorized'
            }
        }

        for index, row in rawdata.iterrows():
            translation_template['Categories'][row['Category']] = ''
            translation_template['Accounts'][row['Account Name']] = ''

    with path_to_translationtemplateJSON.open('w') as translation_template_file:
        json.dump(translation_template, translation_template_file)


def write_account_list(path_to_Book, path_to_accountlistfile):
    """ Write list of accounts in book to a text file"""

    logger.info(f'Function start arguments: {locals()}')

    book = piecash.open_book(path_to_Book.as_posix())

    accountstr = ""
    for account in book.accounts:
        accountstr += account.fullname
        accountstr += '\n'

    path_to_accountlistfile.write_text(accountstr)


##########################################################################
#----GETTING DATA------
##########################################################################


# Open and make translations dictionary
def get_translation(path_to_translationJSON):
    """Returns dictionary with translations.json data"""

    logger.debug(f'Function start arguments: {locals()}')
    with path_to_translationJSON.open() as translationjson:
        return (json.load(translationjson))


def get_rawdata(path_to_rawdata):
    """Returns pandas DF of rawdata csv"""

    logger.debug(f'Function start arguments: {locals()}')
    # Convert missing values to empty strings instead of "nan"
    # For example, "" for Labels and Notes columns
    return (pd.read_csv(path_to_rawdata, na_filter=False))


##########################################################################
#----rawdata TRANSLATING & PREP------
##########################################################################
def translateandprep_rawdata(translation, rawdata):
    """Single function to combine translating_rawdata and preping_rawdata"""

    logger.info(f'Function start')
    return (translating_rawdata(translation, preping_rawdata(rawdata)))


def translating_rawdata(translation, rawdata):
    """Translates the rawdata using translation.json dictionary

    Translates the amount, account, and label of the rawdata based on the dictionary provided by 'translations.json' file. Also filters out transfers that match an 'IGNORE' translation.

    Parameters
    ----------
    translation : dict of dicts
        Holds the tranlastions from rawdata values to GNUCash values. Created using 'translations.json' file.
    rawdata : DataFrame
        The csv data after going through preping_rawdata().

    Returns
    -------
    DataFrame
        csv data with it's contents translated and filtered out.
    """

    logger.info(f'Function start')

    amount_mod = []
    account_mod = []
    category_mod = []
    split_rules = []
    for index, row in rawdata.iterrows():
        logger.debug(f'RAWINDEX={index}, translation started')

        transaction_split_rules = []

        # Make list of transaction values with negatives
        if row['Transaction Type'] == 'debit':
            amount_mod.append(Decimal(str(-row['Amount'])))
        else:
            amount_mod.append(Decimal(str(row['Amount'])))

        # Make list of translated Accounts
        if row['Account Name'] in translation['Accounts'].keys():
            translationValue = translation['Accounts'][row['Account Name']]
            if isinstance(translationValue, dict):
                account = translationValue['Default']
                if 'Categories' in translationValue.keys():
                    if row['Category'] in translationValue['Categories'].keys():
                        account = translationValue['Categories'][row['Category']]
                account_mod.append(account)
            else:
                account_mod.append(translationValue)
        else:
            account_mod.append(translation['Accounts']['Uncategorized'])

        # Make list of translated Categories
        if row['Category'] in translation['Categories'].keys():
            translationValue = translation['Categories'][row['Category']]
            if isinstance(translationValue, dict):
                category = translationValue['Default']
                if 'Accounts' in translationValue.keys():
                    if row['Account Name'] in translationValue['Accounts'].keys():
                        category = translationValue['Accounts'][row['Account Name']]
                category_mod.append(category)
                if 'Splits' in translationValue:
                    transaction_split_rules.extend(translationValue['Splits'])
            else:
                category_mod.append(translationValue)
        else:
            category_mod.append(translation['Categories']['Uncategorized'])

        # Check for split rules in Labels
        for label in row['Labels']:
            if label in translation['Labels'].keys():
                translationValue = translation['Labels'][label]
                if 'Splits' in translationValue:
                    transaction_split_rules.extend(translationValue['Splits'])

        split_rules.append(transaction_split_rules)

        logger.debug(
            f'RAWINDEX={index}, translated values: amount_mod={amount_mod[-1]}, account_mod={account_mod[-1]}, category_mod={category_mod[-1]}'
        )

    rawdata['amount_mod'] = amount_mod
    rawdata['account_mod'] = account_mod
    rawdata['category_mod'] = category_mod
    rawdata['split_rules'] = split_rules

    # If something is translated to 'IGNORE', then it will be dropped from rawdata
    rawdata = rawdata[(rawdata['category_mod'] !=
                       'IGNORE')][(rawdata['account_mod'] != 'IGNORE')]

    # Convert Date column to datetime object
    rawdata['Date'] = rawdata['Date'].apply(
        lambda x: datetime.strptime(x, '%m/%d/%Y'))

    return (rawdata)


def preping_rawdata(rawdata):
    """Finds duplicate transactions and adds 'is_claimed' column"""

    logger.info(f'Function start')

    # See if any duplicates exist. This is to track transfers between equity accounts (ie. internal transactions)
    rawdata['duplicatetf'] = rawdata.duplicated(subset='Amount', keep=False)

    # Add a checkmark column to the data. If True, then the data has been transferred to the transactions_compiled DataFrame
    rawdata['is_claimed'] = False

    # Split the Labels value by spaces into a list
    rawdata['Labels'] = rawdata['Labels'].apply(lambda x: x.split(' '))

    return (rawdata)


##########################################################################
#----------RAW DATA --> gnuCASH COMPATIBLE DATA---------------------------
##########################################################################


def compile_transfers(rawdata, internalCheckAccounts):
    """Returns DataFrame of processed transaction data"""

    logger.info(f'Function start')

    transactions_compiled = pd.DataFrame(
        columns=['description', 'post_date', 'note', 'splits'])

    for index, current_transaction in rawdata.iterrows():
        logger.debug(f'RAWINDEX={index}, Compilation process start')
        if rawdata.at[index, 'is_claimed'] == False:
            # Separating the external transactions from internal. Duplicate indicates internal transaction
            internalTrans_tf = _is_internalTransaction(current_transaction,
                                                       rawdata, index,
                                                       internalCheckAccounts)
            if not internalTrans_tf:
                split_transactions = _determine_splitTransactions(
                    current_transaction, rawdata, index)

                transactions_compiled = _externalTransactions_append(
                    current_transaction, split_transactions, rawdata,
                    transactions_compiled, index)

            # Work on Internal transactions
            elif internalTrans_tf:
                nearest_duplicate = _determine_internalTransactions(
                    current_transaction, rawdata, index, internalCheckAccounts)

                transactions_compiled = _internalTransaction_append(
                    current_transaction, nearest_duplicate, rawdata,
                    transactions_compiled, index)

    return (transactions_compiled)


def _externalTransactions_append(current_transaction, split_transactions, rawdata,
                              transactions_compiled, index):
    """Appends external split transactions to the compiled DataFrame

    Combines the split_transactions into a single transaction statement and appends it to transactions_compiled

    Parameters
    ----------
    current_transaction : DataFrame
        The row that holds the first transaction to be compiled into a single other statement
    split_transactions : DataFrame
        The rows that are splits for the same transaction
    rawdata : DataFrame
        The df that holds the initial data collected (rawdata)
    transactions_compiled : DataFrame
        The df that holds the compiled transaction data

    Returns
    -------
    DataFrame
        The transactions_compiled df with the data from current_transaction appended to it
    """

    logger.debug(f'RAWINDEX={index}, External Transaction (possibly with splits) being appended')

    temp = {'splits': []}

    # Add transaction information to temporary transaction series
    if len(split_transactions) == 1:
        temp['description'] = current_transaction['Description']
    else:
        temp['description'] = current_transaction['Original Description']

    temp['post_date'] = current_transaction['Date']
    # The GNUCash note will be the 'Notes' and 'Original Description' concatenated
    temp['note'] = str(current_transaction['Notes']
                      ) + current_transaction['Original Description']

    # Organizes transfer info between splits
    total_amount = 0
    for rowindex, transaction in split_transactions.iterrows():
        logger.debug(f'RAWINDEX={rowindex}, split {transaction["amount_mod"]}')
        temp['splits'].append({
            'account': transaction['category_mod'],
            'value': -transaction['amount_mod']
        })

        # Add the memo only if it's not already the transaction description.
        if len(split_transactions) != 1:
            temp['splits'][-1]['memo'] = transaction['Description']

        # Apply split rules.
        for split_rule in transaction['split_rules']:
            value = 0
            if split_rule['Amount'] == '+value':
                value = temp['splits'][-1]['value']
            elif split_rule['Amount'] == '-value':
                value = -temp['splits'][-1]['value']

            temp['splits'].append({
                'account': split_rule['Account'],
                'value': value
            })

            # Add the memo only if it's not already the transaction description.
            if len(split_transactions) != 1:
                temp['splits'][-1]['memo'] = transaction['Description']

        rawdata.at[rowindex, 'is_claimed'] = True
        total_amount += transaction['amount_mod']

    logger.debug(f'RAWINDEX={index}, total {total_amount}')
    temp['splits'].append({
        'account': current_transaction['account_mod'],
        'value': total_amount
    })

    transactions_compiled = transactions_compiled.append(
        temp, ignore_index=True)
    logger.debug(f'RAWINDEX={index}, Transaction added successfully')

    return (transactions_compiled)


def _determine_splitTransactions(current_transaction, rawdata, index):
    """Determines the corresponding split transactions to current_transaction"""

    logger.debug(f'RAWINDEX={index}, determining corresponding split transactions')

    # Find all transactions with the same account, original description, and date and haven't been transferred
    split_transactions = rawdata.loc[
        (rawdata['Account Name'] == current_transaction['Account Name']) &
        (rawdata['Original Description'] == current_transaction['Original Description']) &
        (rawdata['Date'] == current_transaction['Date']) &
        (rawdata['is_claimed'] != True)]

    # Filter further: only adjacent transactions can be split
    rows_to_drop = []
    last_rowindex = index - 1
    for rowindex, transaction in split_transactions.iterrows():
        if rowindex == (last_rowindex + 1):
            last_rowindex = rowindex
            logger.debug(f'RAWINDEX={index}, rowindex={rowindex}, amount={transaction["amount_mod"]}')
        else:
            rows_to_drop.append( rowindex )

    split_transactions = split_transactions.drop( rows_to_drop )
    return (split_transactions)


def _internalTransaction_append(current_transaction, nearest_duplicate, rawdata,
                                transactions_compiled, index):
    """Appends internal transaction to transactions_compiled

    Combines the current_transaction and nearest_duplicate into a single internal transaction statement and appends it to transactions_compiled

    Parameters
    ----------
    current_transaction : DataFrame
        The row from rawdata that holds one transaction statement to be compiled into a single other statement
    nearest_duplicate : DataFrame
        Same as current_transaction, but a different row.
    rawdata : DataFrame
        The df that holds the original data from the rawdata
    transactions_compiled : DataFrame
        The df that holds the compiled and processed transaction data

    Returns
    -------
    DataFrame
        The transactions_compiles df with the processed data from current_transaction and nearest_duplicate appended to it.
    """

    logger.debug(f'RAWINDEX={index}, Internal Transaction being appended')

    temp = {'splits': []}

    # Add transaction information to temporary transaction series
    temp[
        'description'] = current_transaction['Description'] + ' ' + nearest_duplicate['Description']
    temp['post_date'] = max(current_transaction['Date'],
                            nearest_duplicate['Date'])
    temp['note'] = str(current_transaction['Notes']
                      ) + current_transaction['Original Description']

    temp['splits'].append({
        'account': current_transaction['account_mod'],
        'value': current_transaction['amount_mod']
    })
    temp['splits'].append({
        'account': nearest_duplicate['account_mod'],
        'value': nearest_duplicate['amount_mod']
    })

    transactions_compiled = transactions_compiled.append(
        temp, ignore_index=True)
    logger.debug(f'RAWINDEX={index}, Transaction added successfully')

    # Mark the transactions has claimed; prevents duplicate in transaction_compiled
    rawdata.at[index, 'is_claimed'] = True
    rawdata.at[nearest_duplicate['rawdataindex'], 'is_claimed'] = True

    # Change category_mod to Internal transaction. Helps distinguish transactions from one another.
    rawdata.at[index, 'category_mod'] = 'Internal Transaction'
    rawdata.at[nearest_duplicate['rawdataindex'],
               'category_mod'] = 'Internal Transaction'

    return (transactions_compiled)


def _determine_internalTransactions(current_transaction, rawdata, index, checkAccounts):
    """Determines the corresponding transaction to current_transaction"""

    logger.debug(f'RAWINDEX={index}, determining corresponding transaction')

    # Find all transactions that have the inverse amount and haven't been transferred
    identical_duplicates = rawdata.loc[
        (rawdata['amount_mod'] == -current_transaction['amount_mod']) &
        (rawdata['is_claimed'] != True)]

    if checkAccounts:
        identical_duplicates = identical_duplicates.loc[
            (identical_duplicates['category_mod'] == current_transaction['account_mod']) &
            (identical_duplicates['account_mod'] == current_transaction['category_mod'])]

    # Note that this assumes that there won't be an identical inverse transaction on the same day
    nearest_duplicate = min(
        identical_duplicates.iterrows(),
        key=lambda x: abs(x[1]['Date'] - current_transaction['Date']))

    # Add the index value of the rawdata. This is to mark it in the 'is_claimed' column of rawdata
    rawdataindex = nearest_duplicate[0]
    nearest_duplicate = nearest_duplicate[1]
    nearest_duplicate['rawdataindex'] = rawdataindex
    logger.debug(
        f'RAWINDEX={index}, Duplicate determined to be at RAWINDEX={rawdataindex}.'
    )

    return (nearest_duplicate)


def _is_internalTransaction(current_transaction, rawdata, index, checkAccounts):
    """Determines whether the current_transaction is internal"""

    logger.debug(f'RAWINDEX={index}, determining transaction type')

    if not current_transaction['duplicatetf']:
        logger.debug(
            f'RAWINDEX={index}, Transfer has no duplicate, therefore external')
        return (False)

    elif current_transaction['duplicatetf']:
        # Find all transactions that have the inverse amount and haven't been transferred
        identical_duplicates = rawdata.loc[
            (rawdata['amount_mod'] == -current_transaction['amount_mod']) &
            (rawdata['is_claimed'] != True)]

        if checkAccounts:
            identical_duplicates = identical_duplicates.loc[
                (identical_duplicates['category_mod'] == current_transaction['account_mod']) &
                (identical_duplicates['account_mod'] == current_transaction['category_mod'])]

        if len(identical_duplicates) != 0:
            logger.debug(
                f'RAWINDEX={index}, Transfer has duplicate and matching transfer, therefore internal'
            )
            return (True)
        else:
            logger.debug(
                f'RAWINDEX={index}, Transfer has duplicate but no matching transfer, therefore external'
            )
            logger.warning(
                f'RAWINDEX={index}, Transfer has duplicate but no matching transfer. Previous matches could\'ve been already claimed. Will assume the transaction is external. Be sure to check the transaction to be sure that it is parsed correctly.'
            )
            return (False)


##########################################################################
#-----------PUTTING DATA IN GNUCASH--------------------------------------
##########################################################################
def import2cash(transactions_compiled, path_to_Book):
    """Write compiled transactions to GNUCash Book"""

    logger.info(f'Function start')
    logger.info(f'Length of transactions_compiled: {len(transactions_compiled.index)}')

    book = piecash.open_book(path_to_Book.as_posix(), readonly=False)

    # if the GNUCash Book has one currency, use that currency
    currencies = [c for c in book.commodities if c.namespace == "CURRENCY"]
    if len(currencies) == 1:
        currency = currencies[0]
    else:
        raise RuntimeError(
            'GNUCash Book must have 1 commodity in the CURRENCY namespace. Please make GitHub issue if this is an issue for you.'
        )

    # create "Uncategorized" account if none exists
    try:
        book.accounts(name='Uncategorized')
    except:
        _ = piecash.Account(
            "Uncategorized", "EXPENSE", currency, parent=book.root_account)
        book.save()

    for index, transaction in transactions_compiled.iterrows():
        logger.debug(
            f'COMPILEDINDEX={index}, writing transaction to GNUCashBook')

        splits = []
        for split in transaction['splits']:
           splits.append( piecash.Split(
                              account=book.accounts(fullname=split['account']),
                              value=split['value'],
                              memo=split.get( 'memo', '' )))

        _ = piecash.Transaction(
            currency=currency,
            description=transaction['description'],
            post_date=transaction['post_date'].date(),
            splits=splits)

    book.save()
